package com.ghigared.example.recyclerviewsampleapp.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.ghigared.example.recyclerviewsampleapp.R;

import static com.ghigared.example.recyclerviewsampleapp.activities.MainActivity.SUB_TEXT_VIEW_KEY;
import static com.ghigared.example.recyclerviewsampleapp.activities.MainActivity.TEXT_VIEW_KEY;

public class InformationDisplayActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_display);
        setInformation();
    }

    private void setInformation(){
        if (getIntent().getExtras() != null){
            TextView textView = (TextView) findViewById(R.id.text_view);
            TextView subTextView = (TextView) findViewById(R.id.sub_text_view);

            textView.setText(getIntent().getExtras().getString(TEXT_VIEW_KEY));
            subTextView.setText(getIntent().getExtras().getString(SUB_TEXT_VIEW_KEY));

        }
    }
}
