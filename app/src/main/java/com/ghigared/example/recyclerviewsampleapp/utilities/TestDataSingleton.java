package com.ghigared.example.recyclerviewsampleapp.utilities;

import com.ghigared.example.recyclerviewsampleapp.R;

import java.util.ArrayList;

public class TestDataSingleton{
    public static TestDataSingleton instance = null;

    public TestDataSingleton(){

    }

    public static TestDataSingleton getInstance(){
        if (instance == null)
            instance = new TestDataSingleton();
        return instance;
    }

    public ArrayList<TestObject> generateTestData(){
        ArrayList<TestObject> tempList = new ArrayList<>();

        tempList.add(new TestObject("Label 1", "Sub Label 1", R.drawable.test_avatar));
        tempList.add(new TestObject("Label 2", "Sub Label 2", R.drawable.test_avatar));
        tempList.add(new TestObject("Label 3", "Sub Label 3", R.drawable.test_avatar));

        return tempList;
    }

    public class TestObject{
        private String label;
        private String subLabel;
        private int imageResource;

        public TestObject(String label, String subLabel, int imageResource) {
            this.label = label;
            this.subLabel = subLabel;
            this.imageResource = imageResource;
        }

        public String getLabel() {
            return label;
        }

        public String getSubLabel() {
            return subLabel;
        }

        public int getImageResource() {
            return imageResource;
        }
    }
}
