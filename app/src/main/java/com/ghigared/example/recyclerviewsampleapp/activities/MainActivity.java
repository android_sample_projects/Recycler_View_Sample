package com.ghigared.example.recyclerviewsampleapp.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ghigared.example.recyclerviewsampleapp.R;
import com.ghigared.example.recyclerviewsampleapp.utilities.TestDataSingleton;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public static String TEXT_VIEW_KEY = "Text View Key";
    public static String SUB_TEXT_VIEW_KEY = "Sub Text View Key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initRecyclerView();
    }

    private void initRecyclerView(){
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new MyAdapter(TestDataSingleton.getInstance().generateTestData()));
    }

    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>{
        private ArrayList<TestDataSingleton.TestObject> objectList;

        public MyAdapter(ArrayList<TestDataSingleton.TestObject> objectList){
            this.objectList = objectList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(getLayoutInflater().inflate(R.layout.recycler_view_row, null));
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.textView.setText(objectList.get(position).getLabel());
            holder.subTextView.setText(objectList.get(position).getSubLabel());
            holder.imageView.setImageResource(objectList.get(position).getImageResource());
        }

        @Override
        public int getItemCount() {
            return objectList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
            public TextView textView;
            public TextView subTextView;
            public ImageView imageView;



            public ViewHolder(View v){
                super(v);
                v.setOnClickListener(this);
                textView = (TextView) v.findViewById(R.id.text_view);
                subTextView = (TextView) v.findViewById(R.id.sub_text_view);
                imageView = (ImageView) v.findViewById(R.id.image_view);
            }

            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(TEXT_VIEW_KEY, textView.getText().toString());
                bundle.putString(SUB_TEXT_VIEW_KEY, subTextView.getText().toString());

                getApplicationContext().startActivity(new Intent(getApplicationContext(),
                        InformationDisplayActivity.class).putExtras(bundle));
            }
        }
    }



}
